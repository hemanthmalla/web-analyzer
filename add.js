var redis = require('redis');
var redisClient = redis.createClient("6379", "localhost");

var ranking = {
	redis_call : function(function_name,key_name,arr,callback){
	    arr.unshift(key_name);
	    redisClient.send_command(function_name,arr,function(err,data){
	        callback(err,data);
	    });
	},
	top_n : function(num){
		this.redis_call("ZREVRANGEBYSCORE","maxheap",["+inf","-inf","LIMIT",0,num],function(err,data){
			console.log(data);
			process.exit();
		});
	},
	set_key: function(count,key){
		this.redis_call("ZADD","maxheap",[count,key],function(err,data){
			console.log(data);
			process.exit();
		});
	},
	add_url : function(url,callback){
		var self = this;
		this.redis_call("ZSCORE","maxheap",[url],function(err,data){
			if(data){
				var count = parseInt(data) + 1;
				self.set_key(count,url);
			}else{
				self.set_key(1,url);
			}

		});
	}
}
exports.ranking = ranking;

(function() {
	if (require.main == module) {
		var arr = process.argv;
		var cmd = arr[2];
		var data = arr[3];
		if(cmd == "add"){
			ranking.add_url(data);
		}else if(cmd == "get"){
			ranking.top_n(data);
		}
	}
}());